using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class DetectarColisiones : MonoBehaviour
{
    public int puntuacionPorEstrella = 10;
    public int puntuacionPorAsteroide = -5;
    public Image Sangre;
    private GameManager gameManager;
    public float velocidadDesvanecimineto = 0.5f;
    private SpriteRenderer spriteRenderer;
    private AudioSource AudioSource;
    [SerializeField] private AudioClip audioClip;
    [SerializeField] private AudioClip audioDisparo;



    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        AudioSource = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        Sangre.gameObject.SetActive(false);
    }


    private void Update()
    {
           
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Estrella"))
        {
            Vibrar();
            other.gameObject.SetActive(false);
            gameManager.AumentarPuntuacion(puntuacionPorEstrella);
        }
        else if (other.CompareTag("Asteroide"))
        {
            if (gameManager.defensaActiva)
            {
                Vibrar();
                other.gameObject.SetActive(false);

            }
            else
            {
                Vibrar();
                other.gameObject.SetActive(false);
                gameManager.RestarPuntaucion(puntuacionPorAsteroide);
                gameManager.PerderVida(1);
                AudioSource.PlayOneShot(audioClip);
                MostrarSangre();
            }

        }
        else if (other.CompareTag("ItemDefensa"))
        {
            gameManager.ActivarDefensa();
            other.gameObject.SetActive(false);
        }
    }


    void Vibrar()
    {

        Handheld.Vibrate();

    }

    void MostrarSangre()
    {
        Sangre.gameObject.SetActive(true);
   
        Color newColor = Sangre.color;
        newColor.a = 1f;
        Sangre.color = newColor;
      
        StartCoroutine(DesvanecerSangre());
    }

    public void DisparoSondio()
    {
        AudioSource.PlayOneShot(audioDisparo);
    }
    IEnumerator DesvanecerSangre()
    {

        yield return new WaitForSeconds(1.5f); 
        while (Sangre.color.a > 0)
        {
            Color newColor = Sangre.color;
            newColor.a -= Time.deltaTime * velocidadDesvanecimineto;
            Sangre.color = newColor;
            yield return null;
        }
      
        Sangre.gameObject.SetActive(false);

    }
}



