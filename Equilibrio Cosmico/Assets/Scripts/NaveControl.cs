using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaveControl : MonoBehaviour
{
    private float speed = 7.0f; // Velocidad de movimiento de la nave
    private float sensitivity = 0.2f; // Sensibilidad del giroscopio
    private float minX = -3.5f; // L�mite m�nimo en X
    private float maxX = 3.5f; // L�mite m�ximo en X
    private float minY = -2f; // L�mite m�nimo en Y
    private float maxY = 3.5f; // L�mite m�ximo en Y
    private Rigidbody rb;

    void Start()
    {
        // Desactiva la gravedad en el Rigidbody
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;

        // Establece la posici�n inicial de la nave en el centro
        transform.position = new Vector3(0.0f, 0.0f, transform.position.z);

        // Habilita el giroscopio si est� disponible
        if (SystemInfo.supportsGyroscope)
        {
            Input.gyro.enabled = true;
        }
        else
        {
            Debug.LogWarning("El dispositivo no soporta giroscopio.");
        }
    }

    void Update()
    {
        Vector3 movement = Vector3.zero;

        // Verifica si el giroscopio est� habilitado
        if (Input.gyro.enabled)
        {
            // Captura la entrada del giroscopio
            Vector3 gyroInput = Input.gyro.rotationRateUnbiased;

            // Ajuste para orientaci�n horizontal (landscape)
            float moveHorizontal = Mathf.Abs(gyroInput.y) > sensitivity ? gyroInput.y : 0;
            float moveVertical = Mathf.Abs(gyroInput.x) > sensitivity ? -gyroInput.x : 0;
           
            // Movimiento de la nave en los ejes X e Y
            movement = new Vector3(moveHorizontal, moveVertical, 0.0f);
           
        }

        // Mover la nave
        transform.Translate(movement * speed * Time.deltaTime);

        // Limitar la posici�n de la nave a los l�mites definidos

        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);
        transform.position = pos;

    }
}
        
    