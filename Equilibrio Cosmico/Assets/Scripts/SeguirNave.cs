using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguirNave : MonoBehaviour
{
    public Transform Nave;
    private Vector3 offset;

    private void Start()
    {
        offset = transform.position - Nave.position;
    }

    private void LateUpdate()
    {
        transform.position = Nave.position + offset;

    }
}
