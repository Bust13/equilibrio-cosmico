using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AparacionSuave : MonoBehaviour
{
    public float fadeDuration = 1f;
    private Renderer rend;
    private Color originalColor;

    void Start()
    {
        rend = GetComponent<Renderer>();
        originalColor = rend.material.color;

       
        Color startColor = originalColor;
        startColor.a = 0f;
        rend.material.color = startColor;

    
        StartCoroutine(FadeIn());
    }

    IEnumerator FadeIn()
    {
        float elapsed = 0f;
        while (elapsed < fadeDuration)
        {
            float alpha = Mathf.Lerp(0f, originalColor.a, elapsed / fadeDuration);
            rend.material.color = new Color(originalColor.r, originalColor.g, originalColor.b, alpha);
            elapsed += Time.deltaTime;
            yield return null;
        }

      
        rend.material.color = originalColor;
    }
}
