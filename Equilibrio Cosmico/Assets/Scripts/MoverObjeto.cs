using UnityEngine;

public class MoverObjeto : MonoBehaviour
{
    private float speed = 15.0f;
    private float lifetime = 5f;
   

    void Update()
    {
        transform.Translate(Vector3.back * speed * Time.deltaTime);
    }
    void OnEnable()
    {
        Invoke("Disable", lifetime);
    }

    void Disable()
    {
        gameObject.SetActive(false);
    }
    void OnDisable()
    {
        CancelInvoke();
    }
}