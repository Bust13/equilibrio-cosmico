
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GestorPuntuacion gestorPuntuacion;
    public Image defensaImage;
    public bool defensaActiva;
    private float defensaTiempo = 5.0f;
    private float defensaContador;
    private bool isPaused = false;
    public List<Image> vidas;
    public GameObject Pausar;
    private int vidasRestantes;
    void Start()
    {
        defensaImage.enabled= false;
        Time.timeScale = 1.0f;
        vidasRestantes = vidas.Count;
    }

    private void Update()
    {
        if(defensaActiva)
        {
            defensaContador-= Time.deltaTime;
            defensaImage.enabled = true;
            if(defensaContador <=0)
            {
               DesactivarDefensa();
            }
        }
    }


    public void AumentarPuntuacion(int puntos)
    {
        gestorPuntuacion.AumentarPuntuacion(puntos);
    }
    public void RestarPuntaucion(int puntos)
    {
        gestorPuntuacion.restarPuntuacion(puntos);
    }

    public void PerderVida(int cantidad)
    {
        if (defensaActiva == false)
        {
           if(vidasRestantes > 0)
            {
                vidasRestantes--;
                vidas[vidasRestantes].enabled = false;
            }
        }

        if (vidasRestantes == 0)
        {
            GameOver();
        }
    }
    public void PonerPusa()
    {
        if (isPaused)
        {
            Time.timeScale = 1.0f;
            isPaused = false;
            Pausar.SetActive(false);
           
        }
        else
        {
            Time.timeScale = 0.0f;
            isPaused=true;
            Pausar.SetActive(true);
         
        }
    }
    void GameOver()
    {
        SceneManager.LoadScene(2);
        Debug.Log("Game over");
    }
    public void ActivarDefensa()
    {
        defensaActiva = true;
        defensaContador = defensaTiempo;
        FindObjectOfType<Defensa>().ActivarEscudo();
    }
    public void DesactivarDefensa()
    {
        defensaActiva = false;
        defensaImage.enabled = false;
        FindObjectOfType<Defensa>().DesactivarEscudo();
    }
    public void Salir()
    {
        Application.Quit();
    }
}
