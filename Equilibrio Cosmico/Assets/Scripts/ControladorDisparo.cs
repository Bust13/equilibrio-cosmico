using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorDisparo : MonoBehaviour
{
    public GameObject proyectilPrefab;
    public Transform puntodDisparoIzq;
    public Transform puntoDisparoDerecho;
    public float velocidadProyetil = 20f;
    public float cadenciaDisparo = 0.5f;
    private float tiempoUltimoDisp;


    public void Disparar()
    {

        if(Time.time>tiempoUltimoDisp + cadenciaDisparo) 
        {
            Debug.Log("Disparando...");
            GameObject proyectilIzquierdo = Instantiate(proyectilPrefab, puntodDisparoIzq.position, puntodDisparoIzq.rotation);
          Rigidbody rbIzquierdo= proyectilIzquierdo.GetComponent<Rigidbody>();
          rbIzquierdo.velocity= puntodDisparoIzq.forward* velocidadProyetil;


          GameObject proyectilDerecho = Instantiate(proyectilPrefab, puntoDisparoDerecho.position, puntoDisparoDerecho.rotation);
          Rigidbody rbDerecho = proyectilDerecho.GetComponent<Rigidbody>();
          rbDerecho.velocity = puntoDisparoDerecho.forward* velocidadProyetil;

          tiempoUltimoDisp = Time.time;
        }
    }
}
