using System.Collections.Generic;
using UnityEngine;

public class GeneradorObjetos : MonoBehaviour
{
   
        public GameObject estrellaPrefab;
        public  List<GameObject> asteroidePrefabs = new List<GameObject>();
        public GameObject itemDefensaPrefab;
        public float spawnRate=1;
        public float minZ = 7f;
        public float maxZ = 12f;

        void Start()
        {
            
            InvokeRepeating("GenerarEstrella", 1f, spawnRate);
            InvokeRepeating("GenerarAsteroide", 2f, spawnRate * 2);
            InvokeRepeating("GenerarItemDefensa", 5f, spawnRate*3);
        }

        void GenerarEstrella()
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-15f, 15f),Random.Range(-10,10f), Random.Range(minZ, maxZ));
            Instantiate(estrellaPrefab, spawnPosition, Quaternion.identity);
        }

        void GenerarAsteroide()
        {
            int randomIndex = Random.Range(0, asteroidePrefabs.Count);
            GameObject asteroidePrefab = asteroidePrefabs[randomIndex];
        Vector3 spawnPosition = new Vector3(Random.Range(-15f, 15f), Random.Range(-10f, 10f), Random.Range(minZ, maxZ));
            Instantiate(asteroidePrefab, spawnPosition, Quaternion.identity);
        }
        void GenerarItemDefensa()
        {
        Vector3 spawnPosition = new Vector3(Random.Range(-15f, 15f), Random.Range(-10f, 10f), Random.Range(minZ, maxZ));
        Instantiate(itemDefensaPrefab, spawnPosition, Quaternion.identity);
        }
}