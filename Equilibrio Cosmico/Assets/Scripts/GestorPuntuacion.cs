using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GestorPuntuacion : MonoBehaviour
{
    public Text puntuacionTexto;
    private int puntuacion; 

    // Start is called before the first frame update
    void Start()
    {
        puntuacion = 0;
        ActualizarPuntuacion();
        
    }
    public void AumentarPuntuacion(int valor)
    { 
        
            puntuacion += valor;
            ActualizarPuntuacion();
        
    }
    public void restarPuntuacion(int valor)
    {
        if (puntuacion >= 5)
        {
            puntuacion += valor;
            ActualizarPuntuacion();
        }
    }
    private void ActualizarPuntuacion()
    {
        puntuacionTexto.text = puntuacion.ToString();
    }
}
