using UnityEngine;

public class SimuladorGiroscopio : MonoBehaviour
{
    public float sensibilidad = 10.0f;

    void Update()
    {
        // Captura la posici�n del mouse
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        // Simula el movimiento del giroscopio
        Vector3 movimientoGyro = new Vector3(-mouseY, mouseX, 0.0f) * sensibilidad;

        // Aplica el movimiento simulado a la nave
        transform.Rotate(movimientoGyro);
    }
}
