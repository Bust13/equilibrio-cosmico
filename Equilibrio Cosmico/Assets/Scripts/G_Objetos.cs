using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class G_Objetos : MonoBehaviour
{
    public float spawnRate = 1;
    private float minZ = 65f;
    private float maxZ = 75f;

    void Start()
    {

        InvokeRepeating("GenerarEstrella", 1f, spawnRate*2);
        InvokeRepeating("GenerarAsteroide", 2f, spawnRate * 1);
        InvokeRepeating("GenerarItemDefensa", 5f, spawnRate * 3);
    }

    void GenerarEstrella()
    {
        Vector3 spawnPosition = new Vector3(Random.Range(-7f, 7f), Random.Range(-5, 5f), Random.Range(minZ, maxZ));
        GenerarObjetosPooling.Instance.SpawnFromPool("Estrella", spawnPosition, Quaternion.identity);
    }
    void GenerarAsteroide()
    {
        int tipoAsteroide = Random.RandomRange(1, 6);
        string tagAsteroide = "Asteroide" + tipoAsteroide;

        Vector3 spawnPosition = new Vector3(Random.Range(-7f, 7f), Random.Range(-5f, 5f), Random.Range(minZ, maxZ));
        GenerarObjetosPooling.Instance.SpawnFromPool(tagAsteroide, spawnPosition, Quaternion.identity);
    }
    void GenerarItemDefensa()
    {
         Vector3 spawnPosition = new Vector3(Random.Range(-7f, 7f), Random.Range(-5f, 5f), Random.Range(minZ, maxZ));
         GenerarObjetosPooling.Instance.SpawnFromPool("ItemDefensa", spawnPosition, Quaternion.identity);
    }

    
}
