using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defensa : MonoBehaviour
{
    public GameManager GameManager;
    public GameObject escudoVisual;

    void Start()
    {
        escudoVisual.SetActive(false);  
    }

    public void ActivarEscudo()
    {
        escudoVisual.SetActive(true);
    }
    public void DesactivarEscudo()
    {
        escudoVisual.SetActive(false);
    }
}
